// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP -- comment each of the two statements below after testing
// =============================================
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

// =============================================
// process.env SETUP , BASE SETUP , new boilerplate
// =============================================
// const PORT = process.env.PORT || 4000; // comment after testing
const { PORT } = process.env; // uncomment after testing
const express = require( 'express' );
const app = express(); 
const path = require('node:path');

// =============================================
// Serve static files from the Webpack app , "catchall" handler to handle all requests in order to return Webpack's index.html file.
// =============================================
app.use( express.static( path.join( __dirname, 'dist' ) ) );
app.get( '*' , ( req, res, next ) => { res.sendFile( path.join( __dirname , 'dist', 'index.html' ) ); } ); 

// =============================================
// BACKEND SERVER INSTANTIATION
// =============================================
app.listen( PORT, () => { 
  // console.log( `Production backend  is running, baby.`); // uncomment after testing
  console.log( `Production backend has started at http://127.0.0.1:${PORT} .`); // comment after testing
} ); // PORT, hostname, backlog (null), cb 