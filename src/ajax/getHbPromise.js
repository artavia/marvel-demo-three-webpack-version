import HBT from './../templates/comic.handlebars'; 

const getHbPromise = () => {

  return new Promise( ( fnresolve, fnreject ) => {

    const goodfunk = () => {
      // console.log('getHbPromise has resolved');
      return HBT;
    };

    fnresolve( goodfunk() );

    // fnreject( new Error("promiseTwo ~ You dun effed things up, slick!!!") );

  } )
  .catch( (error) => { 
    // console.log("error handled" , error );
    return error;
  } );

};

export { getHbPromise }; 