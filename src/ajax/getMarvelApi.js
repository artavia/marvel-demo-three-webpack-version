import { MD5 } from 'crypto-js'; 
import { getUnixTime } from 'date-fns';
// const { PRIVATE, KEY  }  = process.env;

const getMarvelApi = ( year ) => {

  const baseUrl = 'https://gateway.marvel.com:443/v1/public';
  const path = '/comics'; 
  const TS = getUnixTime( new Date() ); // const TS = new Date().getTime();
  const stringToHash = `${TS}${process.env.PRIVATE}${process.env.KEY}`;
  const ALPHAHASH = MD5(stringToHash); // SEE... https://code.google.com/archive/p/crypto-js/wikis/QuickStartGuide_v3beta.wiki 
  const requiredparams = `?apikey=${process.env.KEY}&ts=${TS}&hash=${ALPHAHASH}`; 
  const optionalparams = `&limit=100&format=comic&formatType=comic&dateRange=${year}-01-01%2C${year}-12-31`; 
  const url = `${baseUrl}${path}${requiredparams}${optionalparams}`;
  
  const myRequest = new Request( url );
  const myHeaders = new Headers();  
  myHeaders.append( 'Content-type' , 'application/json; charset=UTF-8' ); 
  myHeaders.append( "Accept" , "application/json" );
  const optionsobject = { headers: myHeaders , method: 'GET' };

  const result = fetch( myRequest , optionsobject )
  .then( response => {
    if (response.status >= 200 && response.status < 300) {
      return Promise.resolve(response); 
    } 
    else {
      let error = new Error( response.statusText );
      return Promise.reject( error ); 
    }
  } )
  .then( data => {
    // console.log( "Promise.resolve(data)", Promise.resolve(data) );
    return Promise.resolve(data.json());
  } )
  .then( result => {
    // console.log( "result", result );
    return result;
  } )
  .catch( err => {
    // console.log( "err", err );
    return Promise.reject( err ); 
  } );

  return result;
};

export { getMarvelApi };