module.exports = function(thumbobjects){
  
  let Handlebars = require('handlebars/dist/handlebars');

  link = Handlebars.Utils.escapeExpression(thumbobjects.link);
  title = Handlebars.Utils.escapeExpression(thumbobjects.title);
  thumb = Handlebars.Utils.escapeExpression(thumbobjects.thumb);

  const result = `
    <a href="${link}" class="comicanchor" title="Link to details about ${title}" target="_blank" rel="noopener noreferrer">
      <img src="${thumb}" class="thumb" alt="Thumbnail picture of ${title}" />
    </a>
  `;

  return new Handlebars.SafeString( result );

};