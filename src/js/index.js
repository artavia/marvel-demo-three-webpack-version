( () => {

  let $results = document.querySelector("#results");
  let $status = document.querySelector("#status");
  let $btnReload = document.querySelector('#btnReload');
  
  // default not avail image
  let IMAGE_NOT_AVAIL = "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available";

  let start = 1988; // let start = 2013;  
  let end = 1986; // let end = 2011;
  
  let deferreds = [];  
  let dataFulfilled = null;
  let asyncHBSTemplate = '';

  
  let getApiModule = ( year ) => {
    return import( /* webpackChunkName: "myCustomCryptoApi" */ './../ajax/getMarvelApi');
  };

  let getTemplateModule = () => {
    return import( /* webpackChunkName: "myCustomExternalHbTemplate" */ './../ajax/getHbPromise');
  }; 

  const thrown = (derrp) => {
    // console.log( "derrp.message" , derrp.message );
    return Promise.reject( derrp.message );
  };

  const getRandomInt = ( min, max ) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor( Math.random() * (max-min+1) ) + min;
  };

  
  
  
  


  
  

  const getComicData = ( year ) => {
    
    // console.log( 'getComicData ~ year', year);

    // return "abc123";
    
    const response = getApiModule( year )
    .then( ({ getMarvelApi }) => {
      
      return getMarvelApi( year )
      .then( json => {
        // console.log( 'json' , json );
        return Promise.resolve( json );
      } )
      .catch( ( err ) => {
        // console.log( "err" , err );
        // console.log( "err.message" , err.message ); 
        return Promise.reject(err);
      } );

    } )
    .catch( ( err ) => { 
      // console.log( "err" , err );
      // console.log( "err.message" , err.message );
      $status.innerHTML = "";
      $status.insertAdjacentHTML( 'beforeend' , err.message ); 
      return Promise.reject(err);
    } );

    return response;

  };

  const promiseOne = () => {

    // console.log( 'deferreds' , deferreds );
    // console.log( 'deferreds.length' , deferreds.length );
    
    if( deferreds.length > 0 ){
      deferreds = [];
    }
    
    return new Promise( ( fnresolve, fnreject ) => {
      
      // let goodfunk = () => {
      //   console.log('The first promise has resolved');
      //   return 'Tutti-frutti';
      // }; 

      const goodfunk = () => {
        $results.innerHTML = '';
        $status.innerHTML = '';

        let initialhtml = '<i>Retrieving your data. Hold please.</i>';
        $status.insertAdjacentHTML('beforeend' , initialhtml ); // $status.innerHTML = initialhtml; 

        for( let x=start; x>=end; x-- ){
          deferreds.push( getComicData(x) );
          // console.log( 'getComicData(' + x + ')' );
        }

        // console.log( 'deferreds' , deferreds );
        // console.log( 'deferreds.length' , deferreds.length );

        return deferreds;
      };

      fnresolve( goodfunk() );
      // fnreject( new Error("promiseOne ~ You dun effed things up, slick!!!") );

    } )
    .catch( (error) => { 
      // console.log("error handled" , error );
      return error;
    } );

  };

  
    

  const templateGrabber = () => {

    const response = getTemplateModule()
    .then( ({ getHbPromise }) => {
      
      return getHbPromise()
      .then( data => {
        // return "123abc";
        // data is EQ. to data = Handlebars.compile(data)
        // e.g. -- data = Handlebars.compile(data);
        asyncHBSTemplate = data;
        // return asyncHBSTemplate;
        return Promise.resolve( asyncHBSTemplate );
      } )
      .catch( ( err ) => {
        // console.log( "err" , err );
        // console.log( "err.message" , err.message ); 
        return Promise.reject(err);  
      } );

    } )
    .catch( ( err ) => {
      // console.log( "err" , err );
      // console.log( "err.message", err.message );
      $status.innerHTML = '';
      $status.insertAdjacentHTML( 'beforeend' , err.message ); 
      return Promise.reject( err ); 
    } );

    return response;

  };

  const extractPromiseObjects = ( pmFulfill ) => {
    if( pmFulfill === null ){
      return pmFulfill = templateGrabber();
    }
  };

  const setupTemplates = ( fulfilledBool ) => {
    // helper is now 'registered' by means of handlebars loader...
    return extractPromiseObjects( fulfilledBool );
  };

  const promiseTwo = () => {
    
    return new Promise( ( fnresolve, fnreject ) => {

      // let goodfunk = () => {
      //   console.log('The second promise has resolved');
      //   return 2000;
      // };

      let config = {};
      config.dataFulfilled = dataFulfilled;

      const goodfunk = ( pm ) => {
        return setupTemplates( pm.dataFulfilled )
      };

      fnresolve( goodfunk( config ) );
      // fnreject( new Error("promiseTwo ~ You dun effed things up, slick!!!") );

    } )
    .catch( (error) => { 
      // console.log("error handled" , error );
      return error;
    } );

  };
  
  const alpha = () => {
    // return Promise.all( [promiseTwo()] );
    return Promise.all( [ promiseOne(), promiseTwo()] );
  };
  
  const bagofcats = async ( response ) => {
    
    // await console.log("bagofcats response" , await response ); // fulfilled OR rejected

    // await console.log("bagofcats typeof response" , await typeof response ); // object
    // await console.log("bagofcats response instanceof Error" , await response instanceof Error ); // false
    // await console.log("bagofcats Array.isArray(response)" , Array.isArray( await response) ); // true

    let comicUrls = [];
    let arrayOfYears = await response[0];

    for( let el of await arrayOfYears ){ 
      
      // await console.log( "el" , await el ); // "attributes/properties" are STILL inaccessible
      
      comicUrls.push( await el );
    }

    // await console.log( "asyncHBSTemplate" , await asyncHBSTemplate );

    return await comicUrls;

  };
  
  const charlie = ( arrayofdata ) => {
    
    // console.log( "arrayofdata" , arrayofdata ); // "attributes/properties" are NOW accessible

    let arrayOfYears = [];
    let arrayOfStats = [];
    let arrayOfResponseObjects = [];

    // $status.innerHTML = ""; // TEST THIS...
    let statushtml = "";
    $status.insertAdjacentHTML("beforeend" , statushtml ); // $status.innerHTML = statushtml; 

    for( let x=0; x<arrayofdata.length; x++ ){
      
      let year = start - x;
      // console.log( 'displaying year' , year ); 

      arrayOfYears.push( year );

      let stats = {};
      stats.year = year;
      stats.priceTotal = 0;
      stats.priceCount = 0;
      stats.minPrice = 999999999;
      stats.maxPrice = -999999999;
      stats.pageTotal = 0;
      stats.pageCount = 0;
      stats.pics = [];
      stats.allTitles = [];
      stats.allDetailUrls = [];

      arrayOfStats.push( stats );

      let res = arrayofdata[x];
      // console.log( '----- res' , res );

      arrayOfResponseObjects.push( res );
    }

    return {
      arrayOfResponseObjects 
      , arrayOfStats
    };

  };

  const delta = ( { arrayOfResponseObjects, arrayOfStats  } ) => {
    
    let arrayOfFilteredItems = [];

    // console.log( "arrayOfResponseObjects" , arrayOfResponseObjects );

    // console.log( "arrayOfStats" , arrayOfStats);

    arrayOfResponseObjects.map( el => {
      
      if(el.code === 200){
        
        let myResults = el.data.results;
        let filtered = myResults.filter( comic => {
          return (comic.thumbnail.path !== IMAGE_NOT_AVAIL) && ( comic.title !== undefined );
        } );

        // console.log( 'filtered' , filtered );
        // console.log( 'filtered.length' , filtered.length );

        arrayOfFilteredItems.push( filtered );
      }

    } );

    return {
      arrayOfStats
      , arrayOfFilteredItems
    };

  };
  
  const elephant = ( { arrayOfStats, arrayOfFilteredItems } ) => { 
    
    // console.log( 'arrayOfStats' , arrayOfStats );
    // console.log( 'arrayOfFilteredItems' , arrayOfFilteredItems );

    arrayOfStats.map( (stat,idx,arr) => {
      
      // console.log( 'stat', stat );

      let mags = arrayOfFilteredItems[idx];
      // console.log( 'mags', mags );

      return mags.map( (comic,idx,arr) => {
        
        // console.log( 'comic', comic );
        if(comic.prices.length && comic.prices[0].price !== 0) {
  
          stat.priceTotal += comic.prices[0].price;

          if(comic.prices[0].price > stat.maxPrice) {
            stat.maxPrice = comic.prices[0].price;
          }

          if(comic.prices[0].price < stat.minPrice) {
            stat.minPrice = comic.prices[0].price;
          }

          stat.priceCount++;
        }

        if(comic.pageCount > 0) {
          stat.pageTotal+=comic.pageCount;
          stat.pageCount++;
        }
        
        // if(comic.thumbnail && comic.thumbnail.path !== IMAGE_NOT_AVAIL) {
          stat.pics.push(`${comic.thumbnail.path}.${comic.thumbnail.extension}`);
        // }

        stat.allTitles.push( comic.title );
        stat.allDetailUrls.push( comic.urls[0].url );

        // console.log( 'stat', stat );
        
        return stat;

      } );

    } );

    // console.log( 'arrayOfStats' , arrayOfStats );
    return arrayOfStats;

  };
  
  const foxtrot = ( arr ) => {
    
    // console.log( 'arr' , arr );

    let stats = arr.map( stat => {
      
      stat.avgPrice = (stat.priceTotal/stat.priceCount).toFixed(2);
      stat.avgPageCount = Math.floor( (stat.pageTotal/stat.pageCount).toFixed(2) );

      stat.thumbs = [];
      stat.titles = [];
      stat.detailUrls = []; 
      stat.thumbobjects = [];
      
      
      // while( stat.pics.length > 0 ) { // pick all thumbnails 
      // while( stat.pics.length > 0 && stat.thumbs.length < 10 ) { // pick 10 thumbnails at random
      // while( stat.pics.length > 0 && stat.thumbs.length < 7 ) { // pick 7 thumbnails at random
      
      while( stat.pics.length > 0 && stat.thumbs.length < 5) { // pick 5 thumbnails at random
        
        let chosen = getRandomInt( 0, stat.pics.length );

        stat.thumbs.push( stat.pics[chosen] );
        stat.pics.splice( chosen, 1 );

        stat.titles.push( stat.allTitles[chosen] );
        stat.allTitles.splice( chosen, 1 );

        stat.detailUrls.push( stat.allDetailUrls[chosen] );
        stat.allDetailUrls.splice( chosen, 1 );

      }

      stat.thumbs.map( (el,idx) => {
        
        let thumbinstance = el;
        let titleinstance = stat.titles[idx];
        let detailurlinstance = stat.detailUrls[idx];
        
        return stat.thumbobjects.push( {
          thumb: thumbinstance
          , title: titleinstance
          , link: detailurlinstance
        } );

      } );
      
      // console.log( 'stat', stat );
      // console.log( 'stat.thumbobjects' , stat.thumbobjects );
      
      return stat;
      
    } );

    return stats;

  };
  
  const golf = ( parsedResults ) => {
    
    // console.log( 'parsedResults' , parsedResults ); 
    // console.log( 'asyncHBSTemplate' , asyncHBSTemplate );
    
    let html = asyncHBSTemplate( { comics: parsedResults } );
    // console.log( 'html' , html );

    $results.insertAdjacentHTML("beforeend" , html ); 
    // $results.innerHTML = html; 
    
  };

  const setupOps = () => { 
    $btnReload.addEventListener( "click", initFunction , false ); 
  };

  const initFunction = () => { 

    // console.log("Konichiwa, dude!!!");
    
    alpha()
    .then( bagofcats )
    .then( charlie )
    .then( delta )
    .then( elephant )
    .then( foxtrot )
    .then( golf )
    .catch( thrown ); 
  
  };

  document.addEventListener( "DOMContentLoaded" , setupOps , false );
  window.addEventListener( "load" , initFunction, false );

} )();