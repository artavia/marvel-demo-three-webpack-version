{
  "name": "marvel-demo-for-webpack-v2-2022",
  "version": "1.0.1",
  "engines": {
    "node": "16.18.1",
    "yarn": "1.22.19"
  },
  "description": "Updated for use with Webpack version 5, a CryptoJS demo but with tree-shakeable ESMs featuring date-fns, postcss-preset-env, crypto-js and Handlebars updated for use subsequent to december 2022.",
  "main": "",
  "keywords": [
    "webpack",
    "handlebars",
    "flexbox",
    "handlebars-loader",
    "ajax",
    "postcss-preset-env",
    "date-fns",
    "crypto-js"
  ],
  "author": "Luis A.",
  "license": "ISC",
  "scripts": { 
    "production": "cross-env webpack --config config/webpack.prod.js",
    "start": "node server.js",
    "heroku-postbuild": "yarn run production"
  },
  "dependencies": {
    "@babel/core": "^7.12.3",
    "@babel/plugin-proposal-class-properties": "^7.12.1",
    "@babel/preset-env": "^7.12.1",
    "babel-loader": "^8.2.1",
    "babel-plugin-date-fns": "^2.0.0",
    "clean-webpack-plugin": "^3.0.0",
    "cross-env": "^7.0.2",
    "crypto-js": "^4.0.0",
    "css-loader": "^5.0.1",
    "css-minimizer-webpack-plugin": "^1.1.5",
    "date-fns": "^2.16.1",
    "express": "^5.0.0-alpha.8",
    "handlebars": "^4.7.6",
    "handlebars-loader": "^1.7.1",
    "html-webpack-plugin": "^5.0.0-alpha.14",
    "mini-css-extract-plugin": "^1.3.1",
    "node-sass": "4.14.1",
    "optimize-css-assets-webpack-plugin": "^5.0.4",
    "postcss": "^8.1.7",
    "postcss-loader": "^4.0.4",
    "postcss-preset-env": "^6.7.0",
    "sass": "^1.29.0",
    "sass-loader": "^10.1.0",
    "style-loader": "^2.0.0",
    "terser-webpack-plugin": "^5.0.3",
    "webpack": "^5.5.1",
    "webpack-cli": "^4.2.0",
    "webpack-dev-server": "^3.11.0",
    "webpack-merge": "^5.4.0"
  }
}

