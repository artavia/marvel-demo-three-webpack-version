# Marvel Demo Three

# Description
Updated for use with Webpack version 5, a CryptoJS demo but with tree-shakeable ESMs featuring date-fns, postcss-preset-env, crypto-js and Handlebars.

## Addendum Number One with Justification
This project has been updated for use subsequent to December 2022. I had five (5) projects simultaneously go to the pot given that Heroku brought Stack 18 to [EOL as explained here](https://help.heroku.com/X5OE6BCA/heroku-18-end-of-life-faq "link to Heroku Help"), hence, a slight re-working of said projects (this is no exception).

## Updated status (2023) ~~Please visit~~
Sorry but subsequent to December 2022 and as a result of the new policy at SalesForce/Heroku, free plans are no longer available to the general public. Thus, the site will be left in an inoperable state. ~~You can see [the lesson at heroku](https://marvel-demo-for-webpack.herokuapp.com/ "the lesson at heroku") and decide if this is something for you to play around with at a later date.~~  

## Final Addendum (2023)
This project utilized Heroku. And it will be currently mothballed as a result of *limited resources*. The five affected projects in question are:
  - [marvel-demo-two-crypto-module-version](https://gitlab.com/artavia/marvel-demo-two-crypto-module-version "link to Gitlab Repository");
  - [marvel-demo-three-webpack-version](https://gitlab.com/artavia/marvel-demo-three-webpack-version "link to Gitlab Repository");
  - [issuetracker-mean-demo](https://gitlab.com/artavia/issuetracker-mean-demo "link to Gitlab Repository");
  - [todotracker-mern-demo](https://gitlab.com/artavia/todotracker-mern-demo "link to Gitlab Repository");
  - [nacho-ordinary-mehn-example](https://gitlab.com/artavia/nacho-ordinary-mehn-example "link to Gitlab Repository");

## The previous version
The [previous repo I had composed did not use Webpack](https://gitlab.com/artavia/marvel-demo-one-cryptojs-version "link to previous repo") but it can be found at Gitlab.

## What makes it unique from anything else I ever slapped together
I sought to take advantage of tree-shaking that only **ECMAScript Modules** are capable of facilitating as opposed to CommonJS. 

I could have used **new Date().getTime()** but I wanted to shake things up a bit by including the **getUnixTime()** method native to **date-fns**. Then, I looked for an excuse to keep practicing with **postcss-preset-env** since I really see its benefits. For its part, I did more digging on the subject of **crypto-js** and it, too, appears to be compatible with **ESM**s. Finally, I want to implement a reload button, hence, I will have my opportunity to lazy-load bundled Webpack output. Therefore, this project produces bundles galore.

Good times!

## Applying the *const* keyword to arrays and objects
Most importantly, **I have applied the *const* keyword to arrays and objects** to make a point and for practical reasons. As I stated during various interviews with *java developers* trying to *explain javascript* to an *experienced javascript developer* such as myself, it is best to apply the **const** keyword as opposed to *let* or *var* since you are changing the references contained within and *not* the actual object whether of type *array* or *object literal*. In other words, the *reference points* contained within the objects are altered-- not the actual value of the variable. 

## This is the part of the presentation where I speak from the top of the soapbox!
You can go now if you do not want to receive a blessing!

What I describe above with the biased interviewers happened to me (again) as recent as the first week of November 2020. I did not get offered the job because of what somebody else's biased opinion (flawed, too, at that) was at that moment. It's crushing to me but everything in life and in death has its purpose.

Either the *hypocrisy* on their part is staggering or the *incompetence* runs deep and is more profound than what the eye can perceive. But I am not surprised given that some people in high places are incapable of using whole sentences in business communications. But I am surprised at the number of people who are over the age of thirty and still have their "mommy" prepare their lunch for them. 

Sometimes you have to laugh in order to keep yourself from falling to pieces. 

If you trust in man, you will be disappointed. But if you trust in God and are righteous in faith, then, it's a whole other story. Also, and not entirely unrelated, mediocrity breeds more mediocrity. Iron will never have the chance to be sharpened against more iron!

Opinions are not facts, thus, I learned to stop worrying a long time ago about what other people think. God chooses my destiny in life; not other people's opinions.

## Today&rsquo;s word&hellip;
I take solace in the Word of God. It gets me through the trials by fire. The latter builds character and the former builds endurance. I want to lead by good example-- so help me God because He shall receive the praise, blessings and glory. 

At the end of the day, we are what we think! What we think is what we believe&hellip; and, what we believe is who we become&hellip;

>
> `For as he thinketh in his heart, so is he...`
> --*Proverbs 23:7*
>
> `Do not speak to fools, for they will scorn your prudent words.`
> --*Proverbs 23:9*
>
> `Do not move an ancient boundary stone or encroach on the fields of the fatherless, for their Defender is strong; he will take up their case against you.`
> --*Proverbs 23:10-11*

## Video(s) of the day
  - A three (3) minute video by my brother from another mother and friend in Christ, [&quot;Georgie&quot; from the *Eternal Call* channel](https://www.youtube.com/watch?v=TAAawH1PYuI "link to youtube") with a video addressed to those that are not yet with us but are ready to find meaning in life;
  - A six (6) minute video by [&quot;Ben Riffs&quot; at youtube](https://www.youtube.com/watch?v=Wb2ln_NXG6I "link to youtube") with a video addressed to those of us (the timing could not have been better) who are resisting the trials set by God;

## Concept and artwork by Luis A.
I hope you enjoy the day that the Lord has prepared for you because He is just and merciful. May you receive the blessings of God our father through his anointed son, Jesus Christ. 