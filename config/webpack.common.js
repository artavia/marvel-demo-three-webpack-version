const webpack = require('webpack');


const paths = require('./paths');

// https://webpack.js.org/plugins/environment-plugin/#dotenvplugin

let dotenv = undefined;
let wdp = undefined;

if( process.env.NODE_ENV !== 'production' ){ 

  dotenv = require('dotenv').config( { 
    path: paths.ROOT_DIR + '/.env'
  } );

  wdp = new webpack.DefinePlugin( {
      
    // SEE https://webpack.js.org/plugins/environment-plugin/
    // 'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV), // EXAMPLE SHYTE
    // 'process.env.DEBUG': JSON.stringify(process.env.DEBUG),    // EXAMPLE SHYTE

    "process.env": JSON.stringify(dotenv.parsed) , 
  } );  
}




const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const cwp = new CleanWebpackPlugin(); 

const HtmlWebpackPlugin = require('html-webpack-plugin');
const hwp = new HtmlWebpackPlugin( { 
  title: 'Marvel Demo Three-- Webpack Version' 
  , template: `${paths.SRC_DIR}/template.html`  // template file 
  , filename: 'index.html'  // output file 
  , favicon: `${paths.SRC_DIR}/images/favicon.ico` // favicon 
} );

// const { loader: minicsspluginloader } = require('mini-css-extract-plugin');

const config = {

  resolve : {
    fallback: { 
      "crypto" : false ,
    }
  } , 

  // BASE SETTINGS

  // entry: paths.SRC_DIR + '/index.js' ,  // DEFAULT VALUE
  entry: {  main: `${paths.SRC_DIR}/index.js` } ,  // DEFAULT VALUE RE-EXPRESSED
  
  plugins: [ 
    
    process.env.NODE_ENV === 'development' ? wdp : null , 
    
    cwp , 
    hwp , 
  ] ,  

  output: { 
    
    // filename: 'main.js'   // #0cjs default - DEFAULT VALUE(s) 
    // filename: '[name].bundle.js'  // CUSTOM VALUE(s) 
    // filename: process.env.NODE_ENV === 'development' ?'[name].js' :'[name].[contenthash].bundle.js' , 
    filename: '[name].js' , 

    path: paths.DIST_DIR , // #0cjs default - DEFAULT VALUE(s)
    
    // Images - PROPOSED NEW 2020
    publicPath: '' ,  // #0cjs default - DEFAULT VALUE(s)
    // , publicPath: '/'  , 

    // Images - PROPOSED NEW 2020

    // https://webpack.js.org/guides/asset-modules/#custom-output-filename
    // https://webpack.js.org/configuration/output/#template-strings

    // assetModuleFilename: 'images/[name][ext][query]'  , // STILL NEEDS TO BE FIXED
    // assetModuleFilename: '[path][name][ext][query]'  , // STILL NEEDS TO BE FIXED
    // assetModuleFilename: '[file]'  , // STILL NEEDS TO BE FIXED 
    assetModuleFilename: '[path][name][ext]' , 

  } , 


  module: { 
    rules: [ 

      // javascript
      // SEE... https://webpack.js.org/loaders/babel-loader/

      { 
        test: /\.js$/ , 
        exclude: /node_modules/ , 
        include: [ `${paths.NODE_DIR}/handlebars/dist` ] ,
        use: {
          loader: 'babel-loader' , 
          options: { 
            presets: [ "@babel/preset-env" ] , 
            
            // plugins: [ 
            //   ["@babel/plugin-proposal-class-properties", { "loose": true } ] , 
            // ] , 

            plugins: [ 

              // https://www.npmjs.com/package/babel-plugin-date-fns
              ["date-fns"] , 

              ["@babel/plugin-proposal-class-properties", { "loose": true } ] , 

            ] , 

            // "date-fns"
          } , 
        }
      
      } , 
      
      // CSS Styles
      // https://webpack.js.org/plugins/css-minimizer-webpack-plugin/#sourcemap
      // https://github.com/webpack-contrib/postcss-loader#sourcemap

      { 
        
        test: /\.(sa|sc|c)ss$/ , 
        
        use: [ 
          
          // process.env.NODE_ENV === 'development' ? { loader: 'style-loader' } : minicsspluginloader , 
          { loader: 'style-loader' } , 
      
          { 
            loader: 'css-loader' , 
            
            options: { 
      
              sourceMap: true , 
      
              // https://github.com/webpack-contrib/css-loader#modules
              modules: false , 
      
              // https://webpack.js.org/loaders/css-loader/#importloaders       
              // importLoaders: 0 , // 0 => no loaders (default);
              // importLoaders: 1 , // 1 => postcss-loader;
              importLoaders: 2 ,    // 2 => postcss-loader, sass-loader
      
            } ,
          } , 
          
          { 
            loader: 'postcss-loader' , 

            options: { 
              
              sourceMap: true , 

              postcssOptions: {

                plugins: [
                  [
                    'postcss-preset-env' , 
                    {
                      // Options
                      
                      // browsers: 'last 2 versions' , 

                      browsers: [ 
                        'last 2 versions' , 
                        // 'last 2 iOS major versions' , 
                        'iOS >= 8' , 
                      ] , 

                      stage: 3 , 

                      features: {
                        'nesting-rules' : true , 
                        'system-ui-font-family' : true , 
                        'rebeccapurple-color' : true , 
                      } , 

                    }
                  ]
                ] , 

              } , 

            } 
            
          } , 

          { 
            loader: 'sass-loader' , 
            options: { 
              sourceMap: true 
            } 
          } , 

        ] ,
        
      } , 
      
      // Images - PROPOSED NEW 2020
      // https://webpack.js.org/guides/asset-modules/#custom-output-filename

      {
        test: /\.(ico|gif|png|jpg|jpeg)$/i ,
        type: 'asset/resource' 
      } , 

      // Images - ANTICIPATED OLD FORMAT
      // {
      //   test : /\.(gif|png|jpg|jpeg)$/ 
      //   , use : [
      //     {
      //       loader : 'file-loader'
      //       , options: { 
      //         name: '[path][name].[ext]' 
              
      //         // , publicPath: '../images/' 
      //         // , outputPath: 'images/' // too complicated
      //         // https://github.com/webpack-contrib/file-loader/issues/160 

      //         , publicPath: function(url) { 
      //           return url.replace( /src\/images/, '../images'); 
      //         } 
      //         , outputPath: function(url) { 
      //           return url.replace( /src\/images/, 'images/'); 
      //         }
      //       }
      //     }
      //   ]
      // } , 
      
      // JSON 
      // {
      //   test: /\.json$/
      //   , loader: 'json-loader'
      //   , type: 'javascript/auto'
      // } , 
      
      // Handlebars 
      {
        test: /\.handlebars$/ , 
        use: [
          {
            loader: 'handlebars-loader' , 
            options: {
              helperDirs: [ `${paths.SRC_DIR}/js/helpers` ]
            }
          }
        ]
      } , 

    ]
  } , 

};

module.exports = config;
// https://webpack.js.org/guides/production/