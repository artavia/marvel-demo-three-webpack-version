const path = require('node:path');

module.exports = {

  // Root folder ICE
  ROOT_DIR: path.resolve( __dirname , '../' ) // .

  // Source files
  , SRC_DIR: path.resolve( __dirname , '../src' ) // ./src
  
  // Production build folder
  , DIST_DIR: path.resolve( __dirname , '../dist' ) // ./dist
  
  // Static files to be copied to the build folder
  , PUBLIC_DIR: path.resolve( __dirname , '../public' ) // ./public
  
  // node_modules package folder
  , NODE_DIR: path.resolve( __dirname , '../node_modules' ) // ./node_modules

};