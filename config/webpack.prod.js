const { merge } = require('webpack-merge');
// const webpack = require('webpack');

const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const paths = require('./paths');
const common = require('./webpack.common.js');

const mcep = new MiniCssExtractPlugin( {
  filename: 'styles/[name].[contenthash].css'
  , chunkFilename: '[id].css'
} );

const cssmin = new CssMinimizerPlugin( {
  // sourceMap: true , // As of Nov 23, 2021... this parameter does not exist 
  minimizerOptions: {
    preset: [ 'default' , { discardComments: { removeAll: true } } ]
  }
} );

const uglification = new TerserPlugin( { 
  test: /\.js(\?.*)?$/i 
  , terserOptions: {
    format: {
      comments: false
    }
  }
  , extractComments: false  // https://webpack.js.org/plugins/terser-webpack-plugin/#remove-comments 
} ); 


const { loader: minicsspluginloader } = require('mini-css-extract-plugin');


const config = {

  output: { 
    
    // filename: 'main.js'   // #0cjs default - DEFAULT VALUE(s) 
    // filename: '[name].bundle.js'  // CUSTOM VALUE(s) 
    // filename: process.env.NODE_ENV === 'development' ?'[name].js' :'[name].[contenthash].bundle.js' , 

    filename: '[name].[contenthash].bundle.js' , 

    path: paths.DIST_DIR , // #0cjs default - DEFAULT VALUE(s)
    
    // Images - PROPOSED NEW 2020
    publicPath: '' ,  // #0cjs default - DEFAULT VALUE(s)
    // , publicPath: '/'  , 

    // Images - PROPOSED NEW 2020

    // https://webpack.js.org/guides/asset-modules/#custom-output-filename
    // https://webpack.js.org/configuration/output/#template-strings

    // assetModuleFilename: 'images/[name][ext][query]'  , // STILL NEEDS TO BE FIXED
    // assetModuleFilename: '[path][name][ext][query]'  , // STILL NEEDS TO BE FIXED
    // assetModuleFilename: '[file]'  , // STILL NEEDS TO BE FIXED 
    assetModuleFilename: '[path][name][ext]' , 

  } , 

  module: { 
    rules: [

      // CSS Styles
      // https://webpack.js.org/plugins/css-minimizer-webpack-plugin/#sourcemap
      // https://github.com/webpack-contrib/postcss-loader#sourcemap

      { 
        
        test: /\.(sa|sc|c)ss$/ , 
        
        use: [ 
          
          minicsspluginloader , 
      
          { 
            loader: 'css-loader' , 
            
            options: { 
      
              sourceMap: true , 
      
              // https://github.com/webpack-contrib/css-loader#modules
              modules: false , 
      
              // https://webpack.js.org/loaders/css-loader/#importloaders       
              // importLoaders: 0 , // 0 => no loaders (default);
              // importLoaders: 1 , // 1 => postcss-loader;
              importLoaders: 2 ,    // 2 => postcss-loader, sass-loader
      
            } ,
          } , 
          
          { 
            loader: 'postcss-loader' , 

            options: { 
              
              sourceMap: true , 

              postcssOptions: {

                plugins: [
                  [
                    'postcss-preset-env' , 
                    {
                      // Options
                      
                      // browsers: 'last 2 versions' , 

                      browsers: [ 
                        'last 2 versions' , 
                        // 'last 2 iOS major versions' , 
                        'iOS >= 8' , 
                      ] , 

                      stage: 3 , 

                      features: {
                        'nesting-rules' : true , 
                        'system-ui-font-family' : true , 
                        'rebeccapurple-color' : true , 
                      } , 

                    }
                  ]
                ] , 

              } , 

            } 
            
          } , 

          { 
            loader: 'sass-loader' , 
            options: { 
              sourceMap: true 
            } 
          } , 

        ] ,
        
      } , 
    ]
  } , 

  plugins: [ 
    mcep , 
  ] ,
  
  mode: "production" , 
  
  devtool: false , 
  
  // https://webpack.js.org/configuration/optimization/

  optimization: { 

    // https://webpack.js.org/configuration/optimization/#optimizationminimize
    
    // Tell webpack to minimize the bundle using the TerserPlugin or the plugin(s) specified in optimization.minimizer (SUCH AS CssMinimizerPlugin)

    minimize: true , 

    minimizer: [ 
      cssmin ,
      uglification , 
    ] , 
    
    // https://webpack.js.org/plugins/split-chunks-plugin/
    
    splitChunks: { 

      // https://webpack.js.org/plugins/split-chunks-plugin/#splitchunkschunks

      chunks: 'all' , 

      // https://webpack.js.org/plugins/split-chunks-plugin/#split-chunks-example-3 
      // SEARCH "// cacheGroupKey here is `commons` as the key of the cacheGroup"
      // THEN, create ADDITIONAL cacheGroups appropriate to handling each DATE-FNS and CRYPTO-JS
      
      cacheGroups: {
        bigotedaliniano: {
          // test: /[\\/]node_modules[\\/](react|react-dom)[\\/]/ ,
          test: /[\\/]node_modules[\\/](handlebars)[\\/](dist)[\\/]/ , 
          // name: 'vendor',
          name: 'bigotedaliniano',
          chunks: 'all',
        } , 
        vendordatefns: {
          // test: /[\\/]node_modules[\\/](react|react-dom)[\\/]/ ,
          test: /[\\/]node_modules[\\/](date-fns)[\\/](getUnixTime)[\\/]/ , 
          name: 'vendordatefns',
          chunks: 'all',
        } , 
        vendorcrypto: {
          // test: /[\\/]node_modules[\\/](react|react-dom)[\\/]/ ,
          test: /[\\/]node_modules[\\/](crypto-js)[\\/]/ ,
          name: 'vendorcrypto',
          chunks: 'all',
        } , 
      } , 

    } , 

    // THIS IS NEW
    // Once your build outputs multiple chunks, this option will ensure they share the webpack runtime
    // instead of having their own. This also helps with long-term caching, since the chunks will only
    // change when actual code changes, not the webpack runtime.
    
    runtimeChunk: {
      name: 'runtime',
    } ,

    // THESE, TOO, ARE ADDITIONAL SETTINGS
    // https://webpack.js.org/configuration/optimization/#optimizationchunkids 
    chunkIds: 'deterministic' , 

    // https://webpack.js.org/configuration/optimization/#optimizationmoduleids 
    moduleIds: 'named' , 

    // https://webpack.js.org/configuration/optimization/#optimizationnodeenv 
    nodeEnv: false , 

    // https://webpack.js.org/configuration/optimization/#optimizationremoveavailablemodules 
    removeAvailableModules: true ,

  } , 

  // https://webpack.js.org/configuration/performance/
  performance: { 
    
    hints: 'error' ,    // 'warning' 'error' false
    // hints: false ,

    maxEntrypointSize: 512000 ,
    maxAssetSize: 512000 ,

  } ,  

};

module.exports = merge( common , config );
// https://webpack.js.org/guides/production/