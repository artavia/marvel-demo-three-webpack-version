const { merge } = require('webpack-merge');
const webpack = require('webpack');

const common = require('./webpack.common.js');
// const paths = require('./paths');

const wpp = new webpack.ProgressPlugin();
const hmrp = new webpack.HotModuleReplacementPlugin();

const config = {

  plugins: [ 
    
    wpp ,
    hmrp , 
  ] ,
  
  mode: "development" , 
  
  // devtool: "source-map" ,  
  devtool: "inline-source-map" , 

  // optimization: {
  //   minimize: false , 
  // } , 
  
  devServer: { 
    
    historyApiFallback: true 
    , open: true
    , compress: true
    , port: 3000
    
    // , contentBase: paths.DIST_DIR // uh, oh! now what!? // As of Nov 23, 2021... this parameter does not exist
    // , hot: true  // As of Nov 23, 2021... "hot: true" automatically applies HMR plugin, you don't have to add it manually to your webpack configuration.

  } , 

  // https://webpack.js.org/configuration/performance/
  performance: {
    
    hints: 'warning' ,    // 'warning' 'error' false
    
    // maxAssetSize: 100000 , 
    // maxEntrypointSize: 400000 ,

    // maxAssetSize: 512000 , 
    // maxEntrypointSize: 512000 , 

    // maxAssetSize: 2048000 , 
    // maxEntrypointSize: 2048000 , 

    // maxAssetSize: 2560000 , 
    // maxEntrypointSize: 2560000 , 

    maxAssetSize: 3072000 , 
    maxEntrypointSize: 3072000 , 

  } ,  
  
  // , 

};

module.exports = merge( common , config );
// https://webpack.js.org/guides/production/